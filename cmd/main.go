package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"net"
	"repository_service/config"
	"repository_service/grpc"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage/postgres"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postgres.New(context.Background(), cfg, log)
	if err != nil {
		log.Error("error is while connecting to db", logger.Error(err))
	}

	services, err := client.NewGrpcClient(cfg, log)
	if err != nil {
		log.Error("error is while initializing grpc client", logger.Error(err))
		return
	}

	grpcServer := grpc.SetUpServer(pgStore, services, log)

	lis, err := net.Listen("tcp", cfg.RepositoryGrpcServiceHost+cfg.RepositoryGrpcServicePort)
	if err != nil {
		log.Error("error is while listening", logger.Error(err))
		return
	}

	log.Info("Service running....", logger.Any("on port:", cfg.RepositoryGrpcServicePort))
	if err := grpcServer.Serve(lis); err != nil {
		log.Error("error is while serving list", logger.Error(err))
	}
}
