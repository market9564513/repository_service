package storage

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
	Income() IIncomeStorage
	IncomeProduct() IIncomeProductStorage
	Storage() IStorageStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pbr.CreateCategory) (*pbr.Category, error)
	Get(context.Context, *pbr.PrimaryKey) (*pbr.Category, error)
	GetList(context.Context, *pbr.GetRequest) (*pbr.CategoriesResponse, error)
	Update(context.Context, *pbr.Category) (*pbr.Category, error)
	Delete(context.Context, *pbr.PrimaryKey) (*empty.Empty, error)
}

type IProductStorage interface {
	Create(context.Context, *pbr.CreateProduct) (*pbr.Product, error)
	Get(context.Context, *pbr.PrimaryKey) (*pbr.Product, error)
	GetList(context.Context, *pbr.GetRequest) (*pbr.ProductsResponse, error)
	Update(context.Context, *pbr.Product) (*pbr.Product, error)
	Delete(context.Context, *pbr.PrimaryKey) (*empty.Empty, error)
	MultipleUpdatePrice(context.Context, *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error)
}

type IIncomeStorage interface {
	Create(context.Context, *pbr.CreateIncome) (*pbr.Income, error)
	Get(context.Context, *pbr.PrimaryKey) (*pbr.Income, error)
	GetList(context.Context, *pbr.GetRequest) (*pbr.IncomesResponse, error)
	Update(context.Context, *pbr.Income) (*pbr.Income, error)
	Delete(context.Context, *pbr.PrimaryKey) (*empty.Empty, error)
}

type IIncomeProductStorage interface {
	Create(context.Context, *pbr.CreateIncomeProducts) (*pbr.IncomeProduct, error)
	Get(context.Context, *pbr.PrimaryKey) (*pbr.IncomeProduct, error)
	GetList(context.Context, *pbr.GetRequest) (*pbr.IncomeProductsResponse, error)
	Update(context.Context, *pbr.IncomeProduct) (*pbr.IncomeProduct, error)
	Delete(context.Context, *pbr.PrimaryKey) (*empty.Empty, error)
}

type IStorageStorage interface {
	Create(context.Context, *pbr.CreateStorage) (*pbr.Storage, error)
	Get(context.Context, *pbr.PrimaryKey) (*pbr.Storage, error)
	GetList(context.Context, *pbr.GetRequest) (*pbr.StoragesResponse, error)
	Update(context.Context, *pbr.Storage) (*pbr.Storage, error)
	Delete(context.Context, *pbr.PrimaryKey) (*empty.Empty, error)
	MultipleCreate(context.Context, *pbr.MultipleRequest) (*pbr.Msg, error)
	MultipleUpdateIncomeProducts(context.Context, *pbr.MultipleUpdateRequest) (*pbr.Msg, error)
	MultipleUpdateSoldProducts(ctx context.Context, request *pbr.MultipleUpdateRequest) (*pbr.Msg, error)
	MultipleUpdatePrice(context.Context, *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error)
}
