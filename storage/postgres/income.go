package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v5/pgxpool"
	pbr "repository_service/genproto/repository_service"
	"repository_service/pkg/generate"
	"repository_service/pkg/helper"
	"repository_service/pkg/logger"
)

type incomeRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewIncomeRepo(db *pgxpool.Pool, log logger.ILogger) *incomeRepo {
	return &incomeRepo{
		db:  db,
		log: log,
	}
}

func (i *incomeRepo) Create(ctx context.Context, income *pbr.CreateIncome) (*pbr.Income, error) {
	var (
		response    = pbr.Income{}
		lastIDQuery = `select id from incomes order by id desc limit 1`
		lastID      string
		incomeID    string
	)

	if err := i.db.QueryRow(ctx, lastIDQuery).Scan(&lastID); err != nil {
		incomeID = generate.IncomeIDGenerate("")
	} else {
		incomeID = generate.IncomeIDGenerate(lastID)
	}

	query := `insert into incomes (id, courier_id, status) values ($1, $2, $3)
                    returning id, courier_id, status, created_at::text`
	if err := i.db.QueryRow(ctx, query,
		incomeID,
		income.GetCourierId(),
		income.GetStatus()).
		Scan(
			&response.Id,
			&response.CourierId,
			&response.Status,
			&response.CreatedAt,
		); err != nil {
		i.log.Error("error is while creating income", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (i *incomeRepo) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Income, error) {
	response := pbr.Income{}

	query := `select id, courier_id, status, created_at::text, updated_at::text from incomes where id = $1 and deleted_at = 0`
	if err := i.db.QueryRow(ctx, query, key.GetId()).
		Scan(
			&response.Id,
			&response.CourierId,
			&response.Status,
			&response.CreatedAt,
			&response.UpdatedAt,
		); err != nil {
		i.log.Error("error is while getting income", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (i *incomeRepo) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.IncomesResponse, error) {
	var (
		incomes []*pbr.Income
		filter  = "where deleted_at = 0 "
		offset  = (request.GetPage() - 1) * request.GetLimit()
		count   = int32(0)
	)

	countQuery := `select count(1) from incomes ` + filter

	if err := i.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		i.log.Error("error while scanning count of incomes", logger.Error(err))
		return nil, err
	}

	query := `select id, courier_id, status, created_at::text, updated_at::text from incomes ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := i.db.Query(ctx, query)
	if err != nil {
		i.log.Error("error while getting incomes rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		income := pbr.Income{}

		if err = rows.Scan(
			&income.Id,
			&income.CourierId,
			&income.Status,
			&income.CreatedAt,
			&income.UpdatedAt,
		); err != nil {
			i.log.Error("error while scanning ony by one income", logger.Error(err))
			return nil, err
		}

		incomes = append(incomes, &income)
	}

	return &pbr.IncomesResponse{Incomes: incomes, Count: count}, nil
}

func (i *incomeRepo) Update(ctx context.Context, income *pbr.Income) (*pbr.Income, error) {
	var (
		resp   = pbr.Income{}
		params = make(map[string]interface{})
		query  = `update incomes set `
		filter = ""
	)

	params["id"] = income.GetId()
	fmt.Println("income id", income.GetId())

	if income.GetCourierId() != "" {
		params["courier_id"] = income.GetCourierId()

		filter += " courier_id = @courier_id,"
	}

	if income.GetStatus() != "" {
		params["status"] = income.GetStatus()

		filter += " status = @status,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, courier_id, status, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := i.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.CourierId,
		&resp.Status,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (i *incomeRepo) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	query := `update incomes set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := i.db.Exec(ctx, query, key.GetId()); err != nil {
		i.log.Error("error is while deleting income", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
