package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbr "repository_service/genproto/repository_service"
	"repository_service/pkg/generate"
	"repository_service/pkg/helper"
	"repository_service/pkg/logger"
	"strings"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) *productRepo {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (p *productRepo) Create(ctx context.Context, product *pbr.CreateProduct) (*pbr.Product, error) {
	response := pbr.Product{}
	barcode := generate.BarcodeGenerate()

	searchingColumn := fmt.Sprintf("%s %s", product.GetName(), barcode)

	query := `insert into products (id, name, category_id, barcode, price, searching_column) values ($1, $2, $3, $4, $5, $6)
                    returning id, name, category_id, barcode, price, created_at::text`
	if err := p.db.QueryRow(ctx, query,
		uuid.New().String(),
		product.GetName(),
		product.GetCategoryId(),
		barcode,
		product.GetPrice(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.Name,
			&response.CategoryId,
			&response.Barcode,
			&response.Price,
			&response.CreatedAt,
		); err != nil {
		fmt.Println("pro", product)
		p.log.Error("error is while creating product", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (p *productRepo) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Product, error) {
	response := pbr.Product{}

	query := `select id, name, category_id, barcode, price, created_at::text, updated_at::text from products where id = $1 and deleted_at = 0`
	if err := p.db.QueryRow(ctx, query, key.GetId()).
		Scan(
			&response.Id,
			&response.Name,
			&response.CategoryId,
			&response.Barcode,
			&response.Price,
			&response.CreatedAt,
			&response.UpdatedAt,
		); err != nil {
		p.log.Error("error is while getting product", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (p *productRepo) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.ProductsResponse, error) {
	var (
		products []*pbr.Product
		filter   = "where deleted_at = 0 "
		offset   = (request.GetPage() - 1) * request.GetLimit()
		count    = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%' ", request.GetSearch())
	}

	countQuery := `select count(1) from products ` + filter

	if err := p.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		p.log.Error("error while scanning count of products", logger.Error(err))
		return nil, err
	}

	query := `select id, name, category_id, barcode, price, created_at::text, updated_at::text from products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := p.db.Query(ctx, query)
	if err != nil {
		p.log.Error("error while getting products rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		product := pbr.Product{}

		if err = rows.Scan(
			&product.Id,
			&product.Name,
			&product.CategoryId,
			&product.Barcode,
			&product.Price,
			&product.CreatedAt,
			&product.UpdatedAt,
		); err != nil {
			p.log.Error("error while scanning ony by one product", logger.Error(err))
			return nil, err
		}

		products = append(products, &product)
	}

	return &pbr.ProductsResponse{Products: products, Count: count}, nil
}

func (p *productRepo) Update(ctx context.Context, product *pbr.Product) (*pbr.Product, error) {
	var (
		resp   = pbr.Product{}
		params = make(map[string]interface{})
		query  = `update products set `
		filter = ""
	)

	params["id"] = product.GetId()
	fmt.Println("product id", product.GetId())

	if product.GetName() != "" {
		params["name"] = product.GetName()

		filter += " name = @name,"
	}

	if product.GetCategoryId() != "" {
		params["category_id"] = product.GetCategoryId()

		filter += " category_id = @category_id,"
	}

	if product.GetPrice() != 0 {
		params["price"] = product.GetPrice()

		filter += " price = @price,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, category_id, barcode, price,created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := p.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Name,
		&resp.CategoryId,
		&resp.Barcode,
		&resp.Price,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (p *productRepo) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	query := `update products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := p.db.Exec(ctx, query, key.GetId()); err != nil {
		p.log.Error("error is while deleting product", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (p *productRepo) MultipleUpdatePrice(ctx context.Context, request *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error) {
	var (
		updateStatements []string
		updateQuery      string
	)
	query := `DO $$ BEGIN %s END $$`
	updateQuery = `update products set price = %d where id = '%s' ;`
	for i, incomePrice := range request.GetPrices() {
		updateStatements = append(updateStatements, fmt.Sprintf(updateQuery, incomePrice, i))
	}

	finalQuery := fmt.Sprintf(query, strings.Join(updateStatements, "\n"))
	if rowsAffected, err := p.db.Exec(ctx, finalQuery); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			p.log.Error("error is while rows affected", logger.Error(err))
			return nil, err
		}
		p.log.Error("error is while updating income products", logger.Error(err))
		return nil, err
	}

	return &pbr.Msg{
		Msg: "prices updated",
	}, nil
}
