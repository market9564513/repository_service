package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbr "repository_service/genproto/repository_service"
	"repository_service/pkg/helper"
	"repository_service/pkg/logger"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) *categoryRepo {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (c *categoryRepo) Create(ctx context.Context, category *pbr.CreateCategory) (*pbr.Category, error) {
	response := pbr.Category{}
	searchingColumn := fmt.Sprintf("%s", category.GetName())

	query := `insert into categories (id, name, parent_id, searching_column) values ($1, $2, $3, $4)
                    returning id, name, parent_id, created_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New(),
		category.GetName(),
		category.GetParentId(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.Name,
			&response.ParentId,
			&response.CreatedAt,
		); err != nil {
		c.log.Error("error is while creating category", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (c *categoryRepo) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Category, error) {
	response := pbr.Category{}

	query := `select id, name, parent_id, created_at::text, updated_at::text from categories where id = $1 and deleted_at = 0`
	if err := c.db.QueryRow(ctx, query, key.GetId()).
		Scan(
			&response.Id,
			&response.Name,
			&response.ParentId,
			&response.CreatedAt,
			&response.UpdatedAt,
		); err != nil {
		c.log.Error("error is while getting category", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (c *categoryRepo) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.CategoriesResponse, error) {
	var (
		categories []*pbr.Category
		filter     = "where deleted_at = 0 "
		offset     = (request.GetPage() - 1) * request.GetLimit()
		count      = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%' ", request.GetSearch())
	}

	countQuery := `select count(1) from categories ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error while scanning count of categories", logger.Error(err))
		return nil, err
	}

	query := `select id, name, parent_id, created_at::text, updated_at::text from categories ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		c.log.Error("error while getting category rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		category := pbr.Category{}

		if err = rows.Scan(
			&category.Id,
			&category.Name,
			&category.ParentId,
			&category.CreatedAt,
			&category.UpdatedAt,
		); err != nil {
			c.log.Error("error while scanning ony by one category", logger.Error(err))
			return nil, err
		}

		categories = append(categories, &category)
	}

	return &pbr.CategoriesResponse{Categories: categories, Count: count}, nil
}

func (c *categoryRepo) Update(ctx context.Context, category *pbr.Category) (*pbr.Category, error) {
	var (
		resp   = pbr.Category{}
		params = make(map[string]interface{})
		query  = `update categories set `
		filter = ""
	)

	params["id"] = category.GetId()
	fmt.Println("category id", category.GetId())

	if category.GetName() != "" {
		params["name"] = category.GetName()

		filter += " name = @name,"
	}

	if category.GetParentId() != "" {
		params["pared_id"] = category.GetParentId()

		filter += " pared_id = @pared_id,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, parent_id, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Name,
		&resp.ParentId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *categoryRepo) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	query := `update categories set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := c.db.Exec(ctx, query, key.GetId()); err != nil {
		c.log.Error("error is while deleting category", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
