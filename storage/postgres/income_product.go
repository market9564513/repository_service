package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbr "repository_service/genproto/repository_service"
	"repository_service/pkg/generate"
	"repository_service/pkg/helper"
	"repository_service/pkg/logger"
)

type incomeProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewIncomeProductRepo(db *pgxpool.Pool, log logger.ILogger) *incomeProductRepo {
	return &incomeProductRepo{
		db:  db,
		log: log,
	}
}

func (i *incomeProductRepo) Create(ctx context.Context, products *pbr.CreateIncomeProducts) (*pbr.IncomeProduct, error) {
	response := pbr.IncomeProduct{}
	barcode := generate.BarcodeGenerate()

	searchingColumn := fmt.Sprintf("%s %s %s", products.GetIncomeId(), products.GetCategoryId(), barcode)

	query := `insert into income_products (id, income_id, category_id, product_id, product_name, barcode, quantity, income_price, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    returning id, income_id, category_id, product_id, product_name, barcode, quantity, income_price, created_at::text`
	if err := i.db.QueryRow(ctx, query,
		uuid.New().String(),
		products.GetIncomeId(),
		products.GetCategoryId(),
		products.GetProductId(),
		products.GetProductName(),
		barcode,
		products.GetQuantity(),
		products.GetIncomePrice(),
		searchingColumn,
	).
		Scan(
			&response.Id,
			&response.IncomeId,
			&response.CategoryId,
			&response.ProductId,
			&response.ProductName,
			&response.Barcode,
			&response.Quantity,
			&response.IncomePrice,
			&response.CreatedAt,
		); err != nil {
		i.log.Error("error is while creating income price", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (i *incomeProductRepo) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.IncomeProduct, error) {
	response := pbr.IncomeProduct{}

	query := `select id, income_id, category_id, product_id, product_name, barcode, quantity, income_price, 
                 created_at::text, updated_at::text from income_products where id = $1 and deleted_at = 0`
	if err := i.db.QueryRow(ctx, query, key.GetId()).
		Scan(
			&response.Id,
			&response.IncomeId,
			&response.CategoryId,
			&response.ProductId,
			&response.ProductName,
			&response.Barcode,
			&response.Quantity,
			&response.IncomePrice,
			&response.CreatedAt,
			&response.UpdatedAt,
		); err != nil {
		i.log.Error("error is while getting income products", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (i *incomeProductRepo) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.IncomeProductsResponse, error) {
	var (
		incomes []*pbr.IncomeProduct
		filter  = "where deleted_at = 0 "
		offset  = (request.GetPage() - 1) * request.GetLimit()
		count   = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%' ", request.GetSearch())
	}

	countQuery := `select count(1) from income_products ` + filter

	if err := i.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		i.log.Error("error while scanning count of incomes", logger.Error(err))
		return nil, err
	}

	query := `select id, income_id, category_id, product_id, product_name, barcode, quantity, income_price,
              created_at::text, updated_at::text from income_products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := i.db.Query(ctx, query)
	if err != nil {
		i.log.Error("error while getting income products rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		income := pbr.IncomeProduct{}

		if err = rows.Scan(
			&income.Id,
			&income.IncomeId,
			&income.CategoryId,
			&income.ProductId,
			&income.ProductName,
			&income.Barcode,
			&income.Quantity,
			&income.IncomePrice,
			&income.CreatedAt,
			&income.UpdatedAt,
		); err != nil {
			i.log.Error("error while scanning ony by one income product", logger.Error(err))
			return nil, err
		}

		incomes = append(incomes, &income)
	}
	return &pbr.IncomeProductsResponse{
		IncomeProducts: incomes,
		Count:          count,
	}, nil
}

func (i *incomeProductRepo) Update(ctx context.Context, income *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	var (
		resp   = pbr.IncomeProduct{}
		params = make(map[string]interface{})
		query  = `update income_products set `
		filter = ""
	)

	params["id"] = income.GetId()
	fmt.Println("income product id", income.GetId())

	if income.GetCategoryId() != "" {
		params["category_id"] = income.GetCategoryId()

		filter += " category_id = @category_id,"
	}

	if income.GetProductId() != "" {
		params["product_id"] = income.GetProductId()

		filter += " product_id = @product_id,"
	}

	if income.GetProductName() != "" {
		params["product_name"] = income.GetProductName()

		filter += " product_name = @product_name,"
	}

	if income.GetQuantity() != 0 {
		params["quantity"] = income.GetQuantity()

		filter += " quantity = @quantity,"
	}

	if income.GetIncomePrice() != 0 {
		params["income_price"] = income.GetIncomePrice()

		filter += " income_price = @income_price,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, income_id, category_id, product_id, product_name, barcode, quantity, income_price, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := i.db.QueryRow(ctx, fullQuery, args...).Scan(
		&income.Id,
		&income.IncomeId,
		&income.CategoryId,
		&income.ProductId,
		&income.ProductName,
		&income.Barcode,
		&income.Quantity,
		&income.IncomePrice,
		&income.CreatedAt,
		&income.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (i *incomeProductRepo) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	query := `update income_products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := i.db.Exec(ctx, query, key.GetId()); err != nil {
		i.log.Error("error is while deleting income product", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
