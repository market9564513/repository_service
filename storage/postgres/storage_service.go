package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbr "repository_service/genproto/repository_service"
	"repository_service/pkg/generate"
	"repository_service/pkg/helper"
	"repository_service/pkg/logger"
	"strings"
)

type storageRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewStorageRepo(db *pgxpool.Pool, log logger.ILogger) *storageRepo {
	return &storageRepo{
		db:  db,
		log: log,
	}
}

func (s *storageRepo) Create(ctx context.Context, storage *pbr.CreateStorage) (*pbr.Storage, error) {
	response := pbr.Storage{}
	barcode := generate.BarcodeGenerate()

	searchingColumn := fmt.Sprintf("%s", barcode)

	query := `insert into storage (id, sale_point_id, product_id, barcode, quantity, income_price, total_price, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8)
                    returning id, sale_point_id, product_id, barcode, quantity, income_price, total_price, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		uuid.New(),
		storage.GetSalePointId(),
		storage.GetProductId(),
		barcode,
		storage.GetQuantity(),
		storage.GetIncomePrice(),
		storage.GetTotalPrice(),
		searchingColumn,
	).
		Scan(
			&response.Id,
			&response.SalePointId,
			&response.ProductId,
			&response.Barcode,
			&response.Quantity,
			&response.IncomePrice,
			&response.TotalPrice,
			&response.CreatedAt,
		); err != nil {
		s.log.Error("error is while creating  storage", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *storageRepo) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Storage, error) {
	response := pbr.Storage{}

	query := `select id, sale_point_id, product_id, barcode, quantity, income_price, total_price, 
                 created_at::text, updated_at::text from storage where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).
		Scan(
			&response.Id,
			&response.SalePointId,
			&response.ProductId,
			&response.Barcode,
			&response.Quantity,
			&response.IncomePrice,
			&response.TotalPrice,
			&response.CreatedAt,
			&response.UpdatedAt,
		); err != nil {
		s.log.Error("error is while getting storage", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *storageRepo) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.StoragesResponse, error) {
	var (
		storages []*pbr.Storage
		filter   = "where deleted_at = 0 "
		offset   = (request.GetPage() - 1) * request.GetLimit()
		count    = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%' ", request.GetSearch())
	}

	countQuery := `select count(1) from storage ` + filter

	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error while scanning count of storage", logger.Error(err))
		return nil, err
	}

	query := `select id, sale_point_id, product_id, barcode, quantity, income_price, total_price,
              created_at::text, updated_at::text from storage ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := s.db.Query(ctx, query)
	if err != nil {
		s.log.Error("error while getting storage rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		storage := pbr.Storage{}

		if err = rows.Scan(
			&storage.Id,
			&storage.SalePointId,
			&storage.ProductId,
			&storage.Barcode,
			&storage.Quantity,
			&storage.IncomePrice,
			&storage.TotalPrice,
			&storage.CreatedAt,
			&storage.UpdatedAt,
		); err != nil {
			s.log.Error("error while scanning ony by one storage", logger.Error(err))
			return nil, err
		}

		storages = append(storages, &storage)
	}
	return &pbr.StoragesResponse{
		Storages: storages,
		Count:    count,
	}, nil
}

func (s *storageRepo) Update(ctx context.Context, storage *pbr.Storage) (*pbr.Storage, error) {
	var (
		response = pbr.Storage{}
		params   = make(map[string]interface{})
		query    = `update storage set `
		filter   = ""
	)

	params["id"] = storage.GetId()
	fmt.Println("storage product id", storage.GetId())

	if storage.GetSalePointId() != "" {
		params["sales_point_id"] = storage.GetSalePointId()

		filter += " sales_point_id = @sales_point_id,"
	}

	if storage.GetProductId() != "" {
		params["product_id"] = storage.GetProductId()

		filter += " product_id = @product_id,"
	}

	if storage.GetQuantity() != 0 {
		params["quantity"] = storage.GetQuantity()

		filter += " quantity = @quantity,"
	}

	if storage.GetIncomePrice() != 0 {
		params["income_price"] = storage.GetIncomePrice()

		filter += " income_price = @income_price,"
	}

	if storage.GetTotalPrice() != 0 {
		params["total_price"] = storage.GetTotalPrice()

		filter += " total_price = @total_price,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id 
             returning id, sale_point_id, product_id, barcode, quantity, income_price, total_price, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.SalePointId,
		&response.ProductId,
		&response.Barcode,
		&response.Quantity,
		&response.IncomePrice,
		&response.TotalPrice,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &response, nil
}

func (s *storageRepo) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	query := `update storage set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := s.db.Exec(ctx, query, key.GetId()); err != nil {
		s.log.Error("error is while deleting storage", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *storageRepo) MultipleCreate(ctx context.Context, request *pbr.MultipleRequest) (*pbr.Msg, error) {
	query := `insert into storage (id, sale_point_id, product_id, barcode, income_price, quantity, total_price) values `

	fmt.Println("request", request.GetNewProducts())
	for _, incomeProducts := range request.GetNewProducts() {
		// total_price
		incomeProducts.TotalPrice += incomeProducts.IncomePrice * incomeProducts.Quantity

		query += fmt.Sprintf(`('%s', '%s', '%s', '%s', %d, %d, %d), `,
			uuid.New().String(),
			incomeProducts.GetSalePointId(),
			incomeProducts.GetProductId(),
			generate.BarcodeGenerate(),
			incomeProducts.GetQuantity(),
			incomeProducts.GetIncomePrice(),
			incomeProducts.GetTotalPrice(),
		)
	}
	fmt.Println("query", query)

	query = query[:len(query)-2]

	if _, err := s.db.Exec(ctx, query); err != nil {
		s.log.Error("error while inserting income products ", logger.Error(err))
		return nil, err
	}

	return &pbr.Msg{Msg: "updated"}, nil
}

func (s *storageRepo) MultipleUpdateIncomeProducts(ctx context.Context, request *pbr.MultipleUpdateRequest) (*pbr.Msg, error) {
	var (
		updateStatements []string
		updateQuery      string
	)

	fmt.Println("request", request.Products)
	query := `DO $$ BEGIN %s END $$`
	updateQuery = `update storage set quantity = quantity + %d where product_id = '%s' ;`
	for _, incomeProducts := range request.GetProducts() {
		fmt.Println("here")
		updateStatements = append(updateStatements, fmt.Sprintf(updateQuery, incomeProducts.Quantity, incomeProducts.ProductId))
	}

	finalQuery := fmt.Sprintf(query, strings.Join(updateStatements, "\n"))
	if rowsAffected, err := s.db.Exec(ctx, finalQuery); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			s.log.Error("error is while rows affected", logger.Error(err))
			return nil, err
		}
		s.log.Error("error is while updating storage", logger.Error(err))
		return nil, err
	}

	fmt.Println("final query", finalQuery)
	return &pbr.Msg{
		Msg: "updated",
	}, nil
}

func (s *storageRepo) MultipleUpdateSoldProducts(ctx context.Context, request *pbr.MultipleUpdateRequest) (*pbr.Msg, error) {
	var (
		updateStatements []string
		updateQuery      string
	)
	query := `DO $$ BEGIN %s END $$`
	updateQuery = `update storage set quantity = quantity - %d where product_id = '%s' ;`
	for _, soldProducts := range request.GetProducts() {
		updateStatements = append(updateStatements, fmt.Sprintf(updateQuery, soldProducts.Quantity, soldProducts.ProductId))
	}

	finalQuery := fmt.Sprintf(query, strings.Join(updateStatements, "\n"))
	if rowsAffected, err := s.db.Exec(ctx, finalQuery); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			s.log.Error("error is while rows affected", logger.Error(err))
			return nil, err
		}
		s.log.Error("error is while updating storage", logger.Error(err))
		return nil, err
	}

	return &pbr.Msg{
		Msg: "updated",
	}, nil
}

func (s *storageRepo) MultipleUpdatePrice(ctx context.Context, request *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error) {
	var (
		updateStatements []string
		updateQuery      string
	)
	query := `DO $$ BEGIN %s END $$`
	updateQuery = `update storage set income_price = %d where product_id = '%s' ;`
	for i, incomePrice := range request.GetPrices() {
		updateStatements = append(updateStatements, fmt.Sprintf(updateQuery, incomePrice, i))
	}

	finalQuery := fmt.Sprintf(query, strings.Join(updateStatements, "\n"))
	if rowsAffected, err := s.db.Exec(ctx, finalQuery); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			s.log.Error("error is while rows affected", logger.Error(err))
			return nil, err
		}
		s.log.Error("error is while updating income products", logger.Error(err))
		return nil, err
	}

	return &pbr.Msg{
		Msg: "prices updated",
	}, nil
}
