package generate

import (
	"fmt"
	"log"
	"strconv"
)

func IncomeIDGenerate(lastID string) string {
	var (
		number            = 1
		randNum, incomeID string
	)

	if lastID == "" {
		randNum = fmt.Sprintf("%06d", number)
		incomeID = "P-" + randNum
		return incomeID
	}

	numberPart := lastID[2:]

	num, err := strconv.Atoi(numberPart)
	if err != nil {
		log.Println("error is while converting income id to int", err)
	}

	num++
	randNum = fmt.Sprintf("%06d", num)
	incomeID = "P-" + randNum

	return incomeID
}
