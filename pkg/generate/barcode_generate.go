package generate

import (
	"fmt"
	"math/rand"
)

func BarcodeGenerate() string {
	barcode := make(map[string]bool)

	for {
		randNum := fmt.Sprintf("%010d", rand.Intn(1000000000))
		numberWithRandom := randNum

		if !barcode[numberWithRandom] {
			barcode[numberWithRandom] = true
			return numberWithRandom
		}
	}
}
