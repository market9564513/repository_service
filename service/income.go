package service

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage"
)

type incomeService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewIncomeService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *incomeService {
	return &incomeService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (i *incomeService) Create(ctx context.Context, income *pbr.CreateIncome) (*pbr.Income, error) {
	return i.storage.Income().Create(ctx, income)
}

func (i *incomeService) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Income, error) {
	return i.storage.Income().Get(ctx, key)
}

func (i *incomeService) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.IncomesResponse, error) {
	return i.storage.Income().GetList(ctx, request)
}

func (i *incomeService) Update(ctx context.Context, income *pbr.Income) (*pbr.Income, error) {
	return i.storage.Income().Update(ctx, income)
}

func (i *incomeService) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	return i.storage.Income().Delete(ctx, key)
}

func (i *incomeService) EndIncome(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Income, error) {
	incomeProductsData, err := i.storage.IncomeProduct().GetList(ctx, &pbr.GetRequest{
		Page:   1,
		Limit:  10,
		Search: key.Id,
	})
	if err != nil {
		i.log.Error("error is while getting income products", logger.Error(err))
		return nil, err
	}

	repo, err := i.storage.Storage().GetList(ctx, &pbr.GetRequest{
		Page:  1,
		Limit: 10,
	})
	if err != nil {
		i.log.Error("error is while getting storage", logger.Error(err))
		return nil, err
	}

	var (
		incomeProducts = make(map[string]*pbr.IncomeProduct)
		newPrices      = make(map[string]int32)
	)

	for _, product := range incomeProductsData.IncomeProducts {
		incomeProducts[product.ProductId] = product
	}

	for _, s := range repo.Storages {
		if incomeProduct, exists := incomeProducts[s.ProductId]; exists {
			fmt.Println("income", incomeProduct)
			if s.IncomePrice != incomeProduct.IncomePrice {
				newPrices[s.ProductId] = s.IncomePrice
				_, err = i.services.StorageService().Update(ctx, &pbr.Storage{
					Id:          s.Id,
					IncomePrice: incomeProducts[s.ProductId].IncomePrice,
				})
				if err != nil {
					i.log.Error("error is while creating new storage product price", logger.Error(err))
					return nil, err
				}
				_, err = i.storage.Product().Update(ctx, &pbr.Product{
					Id:    s.ProductId,
					Price: incomeProducts[s.ProductId].IncomePrice,
				})
				if err != nil {
					i.log.Error("error is while updating new  product price", logger.Error(err))
					return nil, err
				}
			}
			quantity := s.Quantity + incomeProducts[s.ProductId].Quantity
			_, err = i.storage.Storage().Update(ctx, &pbr.Storage{
				Id:         s.Id,
				Quantity:   quantity,
				TotalPrice: s.IncomePrice * quantity,
			})
			if err != nil {
				i.log.Error("error is while adding new income products to storage", logger.Error(err))
				return nil, err
			}
		} else {
			_, err = i.storage.Storage().Create(ctx, &pbr.CreateStorage{
				SalePointId: s.SalePointId,
				ProductId:   incomeProducts[s.ProductId].ProductId,
				ProductName: incomeProducts[s.ProductId].ProductName,
				IncomePrice: incomeProducts[s.ProductId].IncomePrice,
				Quantity:    incomeProducts[s.ProductId].Quantity,
				TotalPrice:  incomeProducts[s.ProductId].IncomePrice * incomeProducts[s.ProductId].Quantity,
			})
			if err != nil {
				fmt.Println("err", err.Error())
				i.log.Error("error is while creating new income product", logger.Error(err))
				return nil, err
			}
		}
	}

	endIncome, err := i.storage.Income().Update(ctx, &pbr.Income{
		Id:     key.Id,
		Status: "finished",
	})
	if err != nil {
		i.log.Error("error is while finishing income", logger.Error(err))
		return nil, err
	}

	return endIncome, nil
}
