package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage"
)

type storageService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewStorageService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *storageService {
	return &storageService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *storageService) Create(ctx context.Context, request *pbr.CreateStorage) (*pbr.Storage, error) {
	request.TotalPrice += request.IncomePrice * request.Quantity
	return s.storage.Storage().Create(ctx, request)
}

func (s *storageService) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Storage, error) {
	return s.storage.Storage().Get(ctx, key)
}

func (s *storageService) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.StoragesResponse, error) {
	return s.storage.Storage().GetList(ctx, request)
}

func (s *storageService) Update(ctx context.Context, request *pbr.Storage) (*pbr.Storage, error) {
	return s.storage.Storage().Update(ctx, request)
}

func (s *storageService) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	return s.storage.Storage().Delete(ctx, key)
}

func (s *storageService) MultipleCreate(ctx context.Context, request *pbr.MultipleRequest) (*pbr.Msg, error) {
	return s.storage.Storage().MultipleCreate(ctx, request)
}

func (s *storageService) MultipleUpdateIncomeProducts(ctx context.Context, request *pbr.MultipleUpdateRequest) (*pbr.Msg, error) {
	return s.storage.Storage().MultipleUpdateIncomeProducts(ctx, request)
}

func (s *storageService) MultipleUpdateSoldProducts(ctx context.Context, request *pbr.MultipleUpdateRequest) (*pbr.Msg, error) {
	return s.storage.Storage().MultipleUpdateSoldProducts(ctx, request)
}

func (s *storageService) MultipleUpdatePrice(ctx context.Context, request *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error) {
	return s.storage.Storage().MultipleUpdatePrice(ctx, request)
}
