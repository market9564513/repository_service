package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *productService {
	return &productService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (p *productService) Create(ctx context.Context, product *pbr.CreateProduct) (*pbr.Product, error) {
	return p.storage.Product().Create(ctx, product)
}

func (p *productService) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Product, error) {
	return p.storage.Product().Get(ctx, key)
}

func (p *productService) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.ProductsResponse, error) {
	return p.storage.Product().GetList(ctx, request)
}

func (p *productService) Update(ctx context.Context, product *pbr.Product) (*pbr.Product, error) {
	return p.storage.Product().Update(ctx, product)
}

func (p *productService) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	return p.storage.Product().Delete(ctx, key)
}

func (p *productService) UpdateMultiplePrice(ctx context.Context, request *pbr.MultipleUpdatePriceRequest) (*pbr.Msg, error) {
	return p.storage.Product().MultipleUpdatePrice(ctx, request)
}
