package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage"
)

type incomeProductService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewIncomeProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *incomeProductService {
	return &incomeProductService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (i *incomeProductService) Create(ctx context.Context, products *pbr.CreateIncomeProducts) (*pbr.IncomeProduct, error) {
	return i.storage.IncomeProduct().Create(ctx, products)
}

func (i *incomeProductService) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.IncomeProduct, error) {
	return i.storage.IncomeProduct().Get(ctx, key)
}

func (i *incomeProductService) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.IncomeProductsResponse, error) {
	return i.storage.IncomeProduct().GetList(ctx, request)
}

func (i *incomeProductService) Update(ctx context.Context, product *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	return i.storage.IncomeProduct().Update(ctx, product)
}

func (i *incomeProductService) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	return i.storage.IncomeProduct().Delete(ctx, key)
}
