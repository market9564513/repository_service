package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/storage"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCategoryService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *categoryService {
	return &categoryService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (c *categoryService) Create(ctx context.Context, category *pbr.CreateCategory) (*pbr.Category, error) {
	return c.storage.Category().Create(ctx, category)
}

func (c *categoryService) Get(ctx context.Context, key *pbr.PrimaryKey) (*pbr.Category, error) {
	return c.storage.Category().Get(ctx, key)
}

func (c *categoryService) GetList(ctx context.Context, request *pbr.GetRequest) (*pbr.CategoriesResponse, error) {
	return c.storage.Category().GetList(ctx, request)
}

func (c *categoryService) Update(ctx context.Context, category *pbr.Category) (*pbr.Category, error) {
	return c.storage.Category().Update(ctx, category)
}

func (c *categoryService) Delete(ctx context.Context, key *pbr.PrimaryKey) (*empty.Empty, error) {
	return c.storage.Category().Delete(ctx, key)
}
