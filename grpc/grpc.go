package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pbr "repository_service/genproto/repository_service"
	"repository_service/grpc/client"
	"repository_service/pkg/logger"
	"repository_service/service"
	"repository_service/storage"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbr.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(storage, services, log))
	pbr.RegisterProductServiceServer(grpcServer, service.NewProductService(storage, services, log))
	pbr.RegisterIncomeServiceServer(grpcServer, service.NewIncomeService(storage, services, log))
	pbr.RegisterIncomeProductServiceServer(grpcServer, service.NewIncomeProductService(storage, services, log))
	pbr.RegisterStorageServiceServer(grpcServer, service.NewStorageService(storage, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
